﻿using System.Collections.ObjectModel;
using Xamarin.Forms;
using System.Threading.Tasks;
using System;
using CloudStorez.Models;
using RestSharp;
using System.Collections.Generic;

namespace CloudStorez
{
	public class OrderListPage:ContentPage
	{
		ListView _orderList;
		ObservableCollection<OrderShort> _orders = new ObservableCollection<OrderShort> ();
		//public List<Order> orderList;
		ToolbarItem _loginToolbarButton;
		ToolbarItem _logoutToolbarButton;
		string _iconName;

		public OrderListPage ()
		{
			LoadOrdersForDisplay ();
			Title = "Order List";
			//ToDo Måste finnas ett mycket bättre sätt än att instansiera det här för att ha en log in/ log out ikon (med tillhörande funktionalitet :) )
			CreateLogoutToolbarButton ();
			CreateLoginToolbarButton ();

			CreateOrderListView ();

			Content = _orderList;
		}

		void CreateLoginToolbarButton ()
		{
			if (_loginToolbarButton != null) {
				return;
			}

			_iconName = Device.OnPlatform (null, "Log_In_icon.png", null);

			_loginToolbarButton = new ToolbarItem ("Login", _iconName, async () => {
				ToolbarItems.Remove (_loginToolbarButton);
				await Navigation.PushAsync (new LoginPage ());
			});
		}

		void CreateLogoutToolbarButton ()
		{
			if (_logoutToolbarButton != null) {
				return;
			}
			_iconName = Device.OnPlatform (null, "Log_Out_icon.png", null);

			_logoutToolbarButton = new ToolbarItem ("Logout", _iconName, async () => {
				ToolbarItems.Remove (_logoutToolbarButton);
				await Navigation.PushModalAsync (new LogoutPage ());
			});
		}

		void LoadOrdersForDisplay ()
		{
			if (App.IsLoggedIn) {
				if (_orders.Count == 0) {
					retrieveOrders ();			
				}
			}
		}

		void CreateOrderListView ()
		{
			_orderList = new ListView {
				
				RowHeight = 60,
				ItemTemplate = new DataTemplate (typeof(OrderCell)),					
			};
			_orderList.ItemSelected += DetailListOnItemSelected;
		}

		async void DetailListOnItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			ListView listView = (ListView)sender;
			if (listView.SelectedItem == null) {
				return;
			}

			await Navigation.PushAsync (new OrderDetailPage ((OrderShort)e.SelectedItem));
			listView.SelectedItem = null;
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			LoadOrdersForDisplay ();
			_orderList.ItemsSource = _orders;

			if (App._currentUser.IsLoggedOn=="OK") {
				//ToDo Bug som lägger till en 'log out' ikon vid 'back' från detaljvyn. Kom ihåg detta när det läggs på fler ikoner i huvudmenyn.
				ToolbarItems.Clear ();
				ToolbarItems.Add (_logoutToolbarButton);
			} else {
				ToolbarItems.Add (_loginToolbarButton);
			}
		}


		//-------------
		public void retrieveOrders ()
		{
			//ToDo Detta ska nog inte ligga i en View-fil...
			var client = new RestClient ("https://ordermanagement-authorized.cloudstorez.com");
			client.CookieContainer = App._currentUser.Cookie;
			var request = new RestRequest ("Orders?organizationId=0a6ca49e-8df7-46d0-ac7a-a46a00816757", Method.GET);
			request.AddHeader ("Content-Type", "application/json; charset=utf-8");
			request.RequestFormat = DataFormat.Json;
			RestResponse<OrderResponse> response = (RestResponse<OrderResponse>)client.Execute<OrderResponse> (request);

			foreach (var realOrder in response.Data.orders) {
				_orders.Add (new OrderShort {
					OrderId = realOrder.orderNumber,
					RegNr = realOrder.properties.licensePlateNumber ?? string.Empty,
					Status = realOrder.orderState,
					TotalPrice = realOrder.totalPrice
				});
			}
		}

	}
}