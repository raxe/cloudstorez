﻿using System.Windows.Input;
using Xamarin.Forms;
using CloudStorez.Models;

namespace CloudStorez
{
	class OrderDetailPage:ContentPage
	{
		readonly ICommand _updateCommand;
		readonly OrderShort _order;

		public OrderDetailPage (OrderShort order)
		{
			_order = order;
			Title = "Order Detail";
			Padding = new Thickness (20);
			BindingContext = order;

			// Create a grid to hold the Labels & Entry controls.
			Grid inputGrid = new Grid {
				ColumnDefinitions = {
					new ColumnDefinition { Width = GridLength.Auto },
					new ColumnDefinition { Width = new GridLength (1, GridUnitType.Star) },
				},
				Children = {
					{ new Label { Text = "Status:", YAlign = TextAlignment.Center, TextColor = Color.Gray }, 0, 0 },
					{ new Label { Text = "Reg.nr:", YAlign = TextAlignment.Center, TextColor = Color.Gray }, 0, 1 },
					{ new Label { Text = "Order Id: ", YAlign = TextAlignment.Center, TextColor = Color.Gray }, 0, 2 },
					{ new Label { Text = "Total Price: ", YAlign = TextAlignment.Center, TextColor = Color.Gray },0,3 },
					{ CreateEntryFor ("Status"), 1, 0 },
					{ CreateLabelFor ("RegNr"), 1, 1 },
					{ CreateLabelFor ("OrderId"), 1, 2 },
					{ CreateEntryFor ("TotalPrice"),1,3 }
				}
			};
			Content = new StackLayout {
				Children = {
					inputGrid
				}
			};
		}

		View CreateEntryFor (string propertyName)
		{
			Entry input = new Entry {
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				TextColor = Color.Accent
			};
			input.SetBinding (Entry.TextProperty, propertyName);
			return input;
		}

		View CreateLabelFor (string propertyName)
		{
			Label label = new Label { 
				HorizontalOptions = LayoutOptions.FillAndExpand,
				FontSize = 18d,
				YAlign = TextAlignment.Center,

			};
			label.SetBinding (Label.TextProperty, propertyName);
			return label;
		}
	}
}



