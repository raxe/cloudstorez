﻿using System;

using Xamarin.Forms;
using CloudStorez.Models;
using RestSharp;

namespace CloudStorez
{
	public class LogoutPage : ContentPage
	{
		

		public LogoutPage ()
		{
			Content = new StackLayout { 
				Children = {
					new Label { Text = "Welcome back another time!" }
				}
			};
		}

		void earseCurrentUser(){

			var loginClient = new RestClient ("https://membership-authorized.cloudstorez.com");
			loginClient.CookieContainer = App._currentUser.Cookie;

			var request = new RestRequest ("user/logoff", Method.POST);
			request.AddHeader ("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

			IRestResponse response = loginClient.Execute (request);

			App._currentUser.IsLoggedOn = "LoggedOutBySelf";
			App._currentUser.Cookie = null;
		}
	}
}


