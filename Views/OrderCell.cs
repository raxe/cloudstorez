﻿using System;
using Xamarin.Forms;

namespace CloudStorez
{
	public class OrderCell:ViewCell
	{
		public OrderCell ()
		{
			Label labelStatus = new Label{ 
				TextColor = Color.Green,
				FontSize = 20d };
			labelStatus.SetBinding (Label.TextProperty, "Status");
				
			Label labelOrderId = new Label {
				VerticalOptions = LayoutOptions.Start
			};
			labelOrderId.SetBinding (Label.TextProperty, "OrderId");

			Label labelDelivery = new Label {
				HorizontalOptions = LayoutOptions.End
			};
			labelDelivery.SetBinding (Label.TextProperty, "TotalPrice");

			Label labelRegNr = new Label {
				FontSize = 16d,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions=LayoutOptions.Center
					
			};
			labelRegNr.SetBinding (Label.TextProperty, "RegNr");


			Grid orderCellGrid = new Grid {
				//VerticalOptions = LayoutOptions.FillAndExpand,
				Padding=new Thickness(20,0,20,0),
				RowDefinitions = {			
					new RowDefinition { Height = GridLength.Auto },
					new RowDefinition { Height = GridLength.Auto }

				},
				ColumnDefinitions = {
					new ColumnDefinition { Width = new GridLength (App.ScreenWidth / 2, GridUnitType.Absolute) },
					new ColumnDefinition { Width = GridLength.Auto },
				},

				Children = {
					{ labelStatus,0,0 },
					{ labelRegNr,1,0 },
					{ labelOrderId,0,1 },
					{ labelDelivery,1,1 }
				}
			};

			View = new StackLayout {
				Children = { orderCellGrid }
			};
		}

		//		public OrderCell ()
		//		{
		//			StackLayout statusAndOrder = CreateStatusAndOrderLayout ();
		//			StackLayout carAndDelivery = CreateCarAndDeliveryLayout ();
		//
		//			StackLayout viewLayout = new StackLayout {
		//				Orientation = StackOrientation.Horizontal,
		//				Children = { statusAndOrder, carAndDelivery }
		//			};
		//			View = viewLayout;
		//		}
		//
		//		static StackLayout CreateCarAndDeliveryLayout ()
		//		{
		//
		//			Label licensePlateLabel = new Label {
		//				FontFamily = "DIN Alternate",
		//				//HorizontalOptions = LayoutAlignment.Center,
		//				HorizontalOptions = LayoutOptions.FillAndExpand
		//			};
		//			licensePlateLabel.SetBinding (Label.TextProperty, "RegNr");
		//
		//			Label deliveryDate = new Label {
		//				HorizontalOptions = LayoutOptions.FillAndExpand
		//
		//			};
		//			deliveryDate.SetBinding (Label.TextProperty, "Delivery");
		//
		//			StackLayout carAndDeliveryLayout = new StackLayout {
		//				HorizontalOptions = LayoutOptions.StartAndExpand,
		//				Orientation = StackOrientation.Vertical,
		//				Children = { licensePlateLabel, deliveryDate }
		//
		//			};
		//			return carAndDeliveryLayout;
		//		}
		//
		//		static StackLayout CreateStatusAndOrderLayout ()
		//		{
		//			Label status = new Label {
		//				HorizontalOptions = LayoutOptions.FillAndExpand,
		//				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label))
		//			};
		//			status.SetBinding (Label.TextProperty, "Status");
		//
		//			Label orderId = new Label {
		//				HorizontalOptions = LayoutOptions.CenterAndExpand
		//			};
		//			orderId.SetBinding (Label.TextProperty, "OrderId");
		//
		//			StackLayout statusAndOrderId = new StackLayout {
		//				HorizontalOptions = LayoutOptions.StartAndExpand,
		//				Orientation = StackOrientation.Vertical,
		//				Children = { status, orderId }
		//			};
		//			return statusAndOrderId;
		//		}
	}
}

