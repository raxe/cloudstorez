﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Net;
using Xamarin.Forms;
using Toasts.Forms.Plugin.Abstractions;

namespace CloudStorez.Models
{
	public class LoginInfo : INotifyPropertyChanged
	{
		Color _loginColour = Color.Gray;
		string _password = string.Empty;
		string _username = string.Empty;
		string _isLoggedOn = string.Empty;
		CookieContainer _cookie;

		public string UserName {
			get { return _username; }
			set {
				if (_username.Equals (value, StringComparison.Ordinal)) {
					return;
				}
				_username = value ?? string.Empty;
				OnPropertyChanged ();

				CanLogin ();
			}
		}

		public string Password {
			get { return _password; }
			set {
				if (_password.Equals (value, StringComparison.Ordinal)) {
					return;
				}
				_password = value ?? string.Empty;
				OnPropertyChanged ();

				CanLogin ();
			}
		}

		public string IsLoggedOn {
			get{ return _isLoggedOn; }
			set { 
				if (value == _isLoggedOn) {
					ShowToast (ToastNotificationType.Info);
					return;
				}
				_isLoggedOn = value;
				if (value == "OK") {
					ShowToast (ToastNotificationType.Success);
				}
			}
		}

		public CookieContainer Cookie { 
			get{ return _cookie;}
			set{ 
				if (value == _cookie) {
					return;
				}
				_cookie = value;
				}
		}

		public Color LoginButtonColour {
			set {
				if (_loginColour == value) {
					return;
				}
				_loginColour = value;
				OnPropertyChanged ();
			}

			get { return _loginColour; }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		///   Logic that determines if the user has entered the proper login information.
		///   Will set the .LoginButtonColour to a Light Blue if the user can login.
		/// </summary>
		/// <returns><c>true</c> if the user can login; otherwise, <c>false</c>.</returns>
		public bool CanLogin ()
		{
			bool login = !string.IsNullOrWhiteSpace (_username) && !string.IsNullOrWhiteSpace (_password);
			LoginButtonColour = login ? Color.Green : Color.Gray;
			return login;
		}

		private async void ShowToast (ToastNotificationType type)
		{
			var notificator = DependencyService.Get<IToastNotificator> ();
			bool tapped = await notificator.Notify (type, "Some " + type.ToString ().ToLower (), "Inne i OrderShort: " + _isLoggedOn, TimeSpan.FromSeconds (2));
		}

		/// <summary>
		///   Helper method that will raise the PropertyChanged event when a property is changed.
		/// </summary>
		/// <param name="propertyName">
		///   Name of the property that was updated. If null then [CallerMemberName] will set it to the name of the
		///   member that invoked it.
		/// </param>
		void OnPropertyChanged ([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) {
				handler (this, new PropertyChangedEventArgs (propertyName));
			}
		}
	}
}
