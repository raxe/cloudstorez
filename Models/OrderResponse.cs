﻿using System;
using System.Collections.Generic;

namespace CloudStorez.Models
{
	public class OrderResponse
	{
		public List<Order> orders { get; set; }
	}
	
	public class OrderItem
	{
		public string id { get; set; }

		public string product { get; set; }

		public string productContainer { get; set; }

		public string productName { get; set; }

		public object productAdditionalName { get; set; }

		public double unitPrice { get; set; }

		public double totalPrice { get; set; }

		public object discount { get; set; }

		public object vat { get; set; }

		public int quantity { get; set; }
	}

	public class OrderEvent
	{
		public string id { get; set; }

		public string eventAction { get; set; }

		public string status { get; set; }

		public object message { get; set; }

		public string date { get; set; }
	}

	public class DeliveryAddress
	{
		public string id { get; set; }

		public object name { get; set; }

		public object coAddress { get; set; }

		public object street { get; set; }

		public object postalCode { get; set; }

		public object city { get; set; }

		public object state { get; set; }

		public object country { get; set; }
	}

	public class BillingAddress
	{
		public string id { get; set; }

		public object name { get; set; }

		public object coAddress { get; set; }

		public object street { get; set; }

		public object postalCode { get; set; }

		public object city { get; set; }

		public object state { get; set; }

		public object country { get; set; }
	}

	public class Properties
	{
		public string licensePlateNumber { get; set; }
	}

	public class Order
	{
		public string id { get; set; }

		public object cart { get; set; }

		public int orderNumber { get; set; }

		public string organization { get; set; }

		public object site { get; set; }

		public object table { get; set; }

		public object seat { get; set; }

		public string user { get; set; }

		public string email { get; set; }

		public string phoneNumber { get; set; }

		public string orderDate { get; set; }

		public string orderState { get; set; }

		public string paymentState { get; set; }

		public double totalPrice { get; set; }

		public string paymentType { get; set; }

		public string deliveryType { get; set; }

		public object message { get; set; }

		public string paymentOption { get; set; }

		public string payment { get; set; }

		public string deliveryOption { get; set; }

		public List<OrderItem> orderItems { get; set; }

		public List<OrderEvent> orderEvents { get; set; }

		public DeliveryAddress deliveryAddress { get; set; }

		public BillingAddress billingAddress { get; set; }

		public bool useFullValidation { get; set; }

		public string requestedDeliveryDateTime { get; set; }

		public string estimatedDeliveryDateTime { get; set; }

		public Properties properties { get; set; }
	}


	
}

