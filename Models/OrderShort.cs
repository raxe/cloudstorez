﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using Toasts.Forms.Plugin.Abstractions;
using Xamarin.Forms;

namespace CloudStorez.Models
{
	public class OrderShort : INotifyPropertyChanged
	{
		int _order;
		string _status;
		double _totalPrice;
		string _regNr;
		string _isLoggedOn;

		public int OrderId {
			get { return _order; }
			set {
				if (value == _order) {
					return;
				}
				_order = value;
				OnPropertyChanged ();
				OnPropertyChanged ("OrderId");
			}
		}

		public string Status {
			get { return _status; }
			set {
				if (value == _status) {
					return;
				}
				_status = value;
				OnPropertyChanged ();
				OnPropertyChanged ("Status");
			}
		}

		public double TotalPrice {
			get { return _totalPrice; }
			set {
				if (value == _totalPrice) {
					return;
				}
				_totalPrice = value;
				OnPropertyChanged ();

			}
		}

		public string RegNr {
			get { return _regNr; }
			set {
				if (value == _regNr) {
					return;
				}
				_regNr = value;
				OnPropertyChanged ();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
		// Required by INotifyPropertyChanged


		protected virtual void OnPropertyChanged ([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) {
				handler (this, new PropertyChangedEventArgs (propertyName));
			}
		}
	}
}
