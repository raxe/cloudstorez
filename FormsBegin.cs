﻿using System;

using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using FormsBegin.Models;

namespace FormsBegin
{
	public class App : Application
	{
		public static int ScreenWidth;
		public static int ScreenHeight;

		public App(){
			MainPage = new NavigationPage (new OrderListPage ());
			//MainPage=new LoginPage();
		}	

		static ObservableCollection<Order> _orders;

		public static bool IsLoggedIn { get; set; }


		/// <summary>
		///   Hard coded list of employees.
		/// </summary>
		/// <value>The employees.</value>
		public static ObservableCollection<Order> Orders
		{
			get
			{
				if (_orders != null)
				{
					return _orders;
				}
				List<Order> list = new List<Order>
				{
					new Order { Status = "New", OrderId = "545191336", RegNr = "ABC 123", Delivery="2015-06-13" },
					new Order { Status = "New", OrderId = "545191337", RegNr = "LOL 432", Delivery="2015-06-17"  },
					new Order { Status = "Confirmed", OrderId = "545191345", RegNr = "DEF 634", Delivery="2015-05-13"  },
					new Order { Status = "Confirmed", OrderId = "545191365", RegNr = "MYCAR", Delivery="2015-06-23"  },
					new Order { Status = "Confirmed", OrderId = "545191325", RegNr = "GHD 332", Delivery="2015-05-03"  }
				};


				_orders = new ObservableCollection<Order>(list.OrderBy(e => e.Status));
				return _orders;
			}
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

