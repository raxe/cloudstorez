﻿using System;

using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using CloudStorez.Models;
using RestSharp;
using System.Net;
using Toasts.Forms.Plugin.Abstractions;
using System.Threading.Tasks;

namespace CloudStorez
{
	public class App : Application
	{
		public static int ScreenWidth;
		public static int ScreenHeight;
		public static LoginInfo _currentUser = new LoginInfo();
		string sd="df";
		public App ()
		{
			MainPage = new NavigationPage (new OrderListPage ());
			//MainPage=new LoginPage();
		}

		public static void DoLogIn (string username, string password)
		{
			var loginClient = new RestClient ("https://membership-authorized.cloudstorez.com");
			loginClient.CookieContainer = new System.Net.CookieContainer ();



			//ToDo for debug purpose only, DO remove :)
			if (username == "q" && password == "a") {
				username = "omer@malmobilproffs.se";
				password = "bilar";
			}

			loginClient.Authenticator = new SimpleAuthenticator ("username", username, "password", password);
			var request = new RestRequest ("user/logon", Method.POST);
			request.AddHeader ("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

			IRestResponse response = loginClient.Execute (request);

			_currentUser.IsLoggedOn = response.StatusDescription;
			//ToDo Probably should keep the loginClient the entire time... Investigate best practise
			_currentUser.Cookie = loginClient.CookieContainer;

		}

		public static bool IsLoggedIn { get; set; }



		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

