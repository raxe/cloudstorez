﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Toasts.Forms.Plugin.Droid;

namespace CloudStorez.Droid
{
	[Activity (Label = "CloudStorez.Droid", Icon = "@drawable/ic_action_content_new", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			//App.ScreenWidth = (int)Resources.DisplayMetrics.WidthPixels; // real pixels
			//App.ScreenHeight = (int)Resources.DisplayMetrics.HeightPixels; // real pixels

			App.ScreenWidth = (int)(Resources.DisplayMetrics.WidthPixels / Resources.DisplayMetrics.Density); // device independent pixels
			App.ScreenHeight = (int)(Resources.DisplayMetrics.HeightPixels / Resources.DisplayMetrics.Density); // device independent pixels

			global::Xamarin.Forms.Forms.Init (this, bundle);
			ToastNotificatorImplementation.Init();

			LoadApplication (new App ());
		}
	}
}

